package com.example.suadahaji.rootdetectionapp;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private static final int ROOT_TRIGGER_COUNT = 3;
    private static boolean HAS_BEEN_ROOTED = false;
    private static boolean ROOT_HAS_BEEN_CHECKED = false;

    public TextView rootDetect;
    public TextView suBinary;
    public TextView rootPackage;
    public TextView rootApks;
    public TextView rootHidingPackage;
    public TextView busyBoxPackage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rootDetect = (TextView) findViewById(R.id.root_detect);
        suBinary = (TextView) findViewById(R.id.su_binary);
        rootPackage = (TextView) findViewById(R.id.root_package);
        rootApks = (TextView) findViewById(R.id.root_apks);
        rootHidingPackage = (TextView) findViewById(R.id.root_hiding_package);
        busyBoxPackage = (TextView) findViewById(R.id.busy_box_package);

        if (isDeviceRooted()) {
            rootDetect.setText("Root detected");
            return;
        } else {
            rootDetect.setText("Root not detected");
        }
    }

    public boolean isDeviceRooted() {
        if (!isRootDetectionEnabled()) {
            return false;
        }

        if (ROOT_HAS_BEEN_CHECKED) {
            return HAS_BEEN_ROOTED;
        }

        int rootTriggers = 0;

        if (checkForSuBinary()) {
            rootTriggers++;
        }

        if (checkIfRootAPKsExist()) {
            rootTriggers++;
        }

        if (checkIfRootPackagesAreInstalled(this)) {
            rootTriggers++;
        }

        if (checkIfRootHidingPackagesAreInstalled(this)) {
            rootTriggers++;
        }

        if (checkIfBusyBoxPackagesAreInstalled(this)) {
            rootTriggers++;
        }

        ROOT_HAS_BEEN_CHECKED = true;
        if (rootTriggers >= ROOT_TRIGGER_COUNT) {
            HAS_BEEN_ROOTED = true;
        }

        return HAS_BEEN_ROOTED;
    }

    private boolean checkForSuBinary() {
        String[] paths = {"/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su"};
        StringBuilder builder = new StringBuilder();
        for (String path : paths) {
            builder.append(path);
            builder.append("\n ");
            if (new File(path).exists()) {
                suBinary.setText("Root detected: su file found at \n" + builder.toString().trim());
                Log.d(TAG, "Root detected: su file found at " + path);
                return true;
            } else {
                suBinary.setText("Su file not found.");
            }
        }
        return false;
    }

    private boolean checkIfRootAPKsExist() {
        String[] files = {"/system/app/Superuser.apk", "/system/app/Superuser-1.apk", "/system/app/eu.chainfire.supersu.apk", "/system/app/eu.chainfire.supersu-1.apk", "/system/app/com.noshufou.android.su.apk", "/system/app/com.noshufou.android.su-1.apk", "/system/app/com.thirdparty.superuser.apk", "/system/app/com.thirdparty.superuser-1.apk", "/system/app/com.koushikdutta.superuser.apk", "/system/app/com.koushikdutta.superuser-1.apk"};
        StringBuilder builder = new StringBuilder();
        for (String file : files) {
            builder.append(file);
            builder.append("\n ");
            if (new File(file).exists()) {
                rootApks.setText("Root detected: Root APK file found at: \n" + builder.toString().trim());
                Log.d(TAG, "Root detected: Root APK file found at " + file);
                return true;
            } else {
                rootApks.setText("Root APK file not found.");
            }
        }
        return false;
    }

    private boolean checkIfRootPackagesAreInstalled(Context context) {
        String[] packagesToCheck = {"eu.chainfire.supersu", "com.noshufou.android.su", "com.thirdparty.superuser", "com.koushikdutta.superuser"};
        StringBuilder builder = new StringBuilder();
        for (String packageName : packagesToCheck) {
            builder.append(packageName);
            builder.append("\n ");
            if (isPackageInstalled(context, packageName)) {
                rootPackage.setText("Root packages installed include: " + builder.toString().trim());
                Log.d(TAG, "Root detected: Root package found - " + packageName);
                return true;
            } else {
                rootPackage.setText("Root packages not installed.");
            }
        }
        return false;
    }

    private boolean checkIfRootHidingPackagesAreInstalled(Context context) {
        String[] packagesToCheck = {"com.zachspong.temprootremovejb", "com.ramdroid.appquarantine"};
        StringBuilder builder = new StringBuilder();
        for (String packageName : packagesToCheck) {
            builder.append(packageName);
            builder.append("\n ");
            if (isPackageInstalled(context, packageName)) {
                rootHidingPackage.setText("Root detected: Root hiding package found - " + builder.toString().trim());
                Log.d(TAG, "Root detected: Root hiding package found - " + packageName);
                return true;
            } else {
                rootHidingPackage.setText("Root hiding package not found.");
            }
        }
        return false;
    }

    private boolean checkIfBusyBoxPackagesAreInstalled(Context context) {
        String[] packagesToCheck = {"stericson.busybox"};
        for (String packageName : packagesToCheck) {
            if (isPackageInstalled(context, packageName)) {
                busyBoxPackage.setText("BusyBox package found at  " + packageName);
                Log.d(TAG, "Root detected: BusyBox package found");
                return true;
            } else {
                busyBoxPackage.setText(" BusyBox package not found.");
            }
        }
        return false;
    }

    public boolean isRootDetectionEnabled() {
        return true;
    }

    public static boolean isPackageInstalled(Context context, String packageName) {
        return getPackageInfo(context, packageName) != null;
    }

    public static PackageInfo getPackageInfo(Context context, String packageName) {
        PackageManager packageManager = context.getPackageManager();
        try {
            return packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException ignored) {

        }
        return null;
    }
}
